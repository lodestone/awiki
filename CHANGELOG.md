## 0.1.0 - First Release
* Basic implementation of links
* Basic implementation of wiki index

## 0.2.0
* Added ability to create links by pressing `alt-enter`
