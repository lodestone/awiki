---
title: Exploring the Possibilities of a Writing Outline Using File Transclusion  
author: Shane M. Willson  
date: 150612  
bibliography: /Users/shanewillson/Dropbox/000-lit/000-bibtex/000-master-bibtex-smw.bib  
bibtex: /Users/shanewillson/Dropbox/000-lit/000-bibtex/000-master-bibtex-smw  
abstract: This document is an attempt to create a hypertextual outline of zettel notes. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.  
...





# A Couple of Zettels Notes Transcluded In

## Here's a Good Little Note on TTs as *Boundary Organizations*

---

{{nts-zettel-test-1.txt}}

---


## Methods and Data Gathering

This fella acquired financial info on 30 think tanks by using a perl script to scrape publicly available databases. TTs have to file forms to stay tax exempt.

---

{{Dropbox/000-notes/nts-zettel-test-2.txt}}

---



# Using MMD4's 'File Transclusion'to Generate Hypertextual Outlines of Zettel Notes

This is an example of an outline that could be used for our hypertext notes system. 

Find and replace the following with your own notes to see if it works for you.

	Dropbox/000-notes/nts-zettel-test-1.txt
	Dropbox/000-notes/nts-zettel-test-2.txt
	

There's a good bit of junk in this document, but check these parts out:

[Insert Contents of a File]

[The Reason All This Stuff Is Important]


## The Basics and Syntax 


The syntax and shit comes from Fletcher Penny. 

[Fletcher's Guide to File Transclusion](http://fletcher.github.io/MultiMarkdown-4/transclusion.html)

Or is it 'inclusion'? I dunno.


### Insert Contents of a File

Let's tell MultiMarkdown to insert the contents of another file inside the current file being processed. 

If a file named some_other_file.txt exists, its contents will be inserted inside of this document before being processed by MultiMarkdown. This means that the contents of the file can also contain MultiMarkdown formatted text.


This is some text.

But if I put in the following code...

	{{Dropbox/000-notes/zettel-test-1.txt}}

the contents of the included file will be processed. 

---

**The contents of the aforementioned file should be below this line.**


{{Dropbox/000-notes/books-to-read.txt}}


**This is where the original document starts back up.**

---


Pretty fuckin' cool, huh?

But wait...

**THERE'S MOAR!!!**


### Display Contents of File W/O Processing


This is some text. Coming up, we should see the contents of the included file, but they shouldn't be processed. (Now I'm just having fun. But this could be super-cool for sweaving.)



```
{{Dropbox/000-notes/books-to-read.txt}}
```

Another paragraph



#### The Rest of Fletcher's Explanation

Transclusion is recursive, so the file being inserted will be scanned to see if it references any other files.

Metadata in the file being inserted will be ignored. This means that the file can contain certain metadata when viewed alone that will not be included when the file is transcluded by another file.


*I should probably try the stuff below...*

You can use the `[Transclude Base]` metadata to specify where MultiMarkdown should look for the files to be included. All files must be in this folder. If this folder is not specified, then MultiMarkdown will look in the same folder as the parent file.

### A Quick Failed Test of the MetaData Variables

This is foo: [%foo]. 

The date of this note is [%date].

[%title]

[%abstract]



# The Reason All This Stuff Is Important


Imagine that I have a bunch of little notes that refer to important shit that will help me write my paper. While it's cool to have a flat pile of files, I'm a visual person. Also, most papers start with an outline. So, we need a way to keep our flat notes database (since it's nice to know that everything is there when searching for new notes), while also having the ability to add notes to specific projects and outlines *without duplicating them*.



## An Outline of Notes That Will Help Me Write a Paper (Maybe)


# Heading One

## A Subheading about a Topic

### On Groceries:

The following note is really relevant to **Groceries**.

{{Dropbox/000-notes/groceries.txt}}


### On Books to Read:

This note is super-useful for books I want to read:

{{Dropbox/000-notes/books-to-read.txt}}


## Two Notes

Groceries are very important for sociologists to understand. Shane has a grocery list.

{{Dropbox/000-notes/groceries.txt}}

Reading books is also important to my thesis. The following books are in my "Books to Read" file. 

{{Dropbox/000-notes/books-to-read.txt}}


### `Transclude` Does Not Seem to Work With Lists

- Heading One
	- A Subheading about a Topic
		- On Groceries:
			- {{Dropbox/000-notes/groceries.txt}}
		- On Books to Read:
			- {{Dropbox/000-notes/books-to-read.txt}}


## Conclusion

If we take small, "atomic" notes (i.e. "one idea per note") and keep them all in our **Notes Database** (a notes folder on Dropbox with nvAlt pointed at it), we can keep them in that database, but still attach them to multiple writing projects/outlines *without creating duplicates*. 

After we've taken some notes and made a general outline, we can hunt through nvAlt using keywords and tags and shit and find stuff that's relevant to our outline. 

Then, we flesh out our outline with included note files. Once the outline is fleshed out, we run it through Marked and end up with a "Writing Outline" that makes it easy to write our papers. 


**BOOSH!!!**


	/me drops the mic and walks out.


{{Dropbox/000-notes/mmd-footer-file.txt}}

